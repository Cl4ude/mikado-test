import { Subject } from 'rxjs';

export class FilmsService {

    private films: string[] = ['film 1', 'film 2', 'film 3'];

    fimsSubject =  new Subject<string[]>();

    emitFilms() {
        this.fimsSubject.next(this.films);
    }
}
